package com.finaltest.levels.impl;

import org.apache.logging.log4j.Level;

import com.finaltest.levels.LogLevelable;

public class LogLevels implements LogLevelable{

	public void createLevels() {
		Level.forName("MUSTFIX", 1);
		Level.forName("DATABASE", 250);
		Level.forName("FAILOVER", 350);
		Level.forName("FIXLATER", 450);
		Level.forName("MYDEBUG", 550);
	}

}
