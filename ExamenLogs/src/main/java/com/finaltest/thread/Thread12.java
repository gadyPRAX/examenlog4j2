package com.finaltest.thread;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

public class Thread12 extends Thread {
	private static final Logger logger = LogManager.getLogger();
	
	public Thread12(String name) {
		super(name);
	}
	
	@Override
	public void run() {
		ThreadContext.put("name", "Edgar Garcia");
		ThreadContext.put("userName", "Nassus");
		logger.trace("Logging TRACE");
	}
}
