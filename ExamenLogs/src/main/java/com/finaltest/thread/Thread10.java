package com.finaltest.thread;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

public class Thread10 extends Thread {
	private static final Logger logger = LogManager.getLogger();
	
	public Thread10(String name) {
		super(name);
	}
	
	@Override
	public void run() {
		ThreadContext.put("name", "Edgar Garcia");
		ThreadContext.put("userName", "Nassus");
		logger.log(Level.getLevel("FIXLATER"),"Logging FIXLATER");
	}
}
