package com.finaltest.thread;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

public class Thread3 extends Thread {
	private static final Logger logger = LogManager.getLogger();
	
	public Thread3(String name) {
		super(name);
	}
	
	@Override
	public void run() {
		ThreadContext.put("name", "Edgar Garcia");
		ThreadContext.put("userName", "Nassus");
		logger.info("Logging info");
	}
}
